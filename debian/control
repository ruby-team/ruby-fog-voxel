Source: ruby-fog-voxel
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Balasankar C <balasankarc@autistici.org>
Build-Depends: debhelper (>= 9~),
               gem2deb,
               rake,
               ruby-fog-core,
               ruby-fog-xml,
               ruby-rspec,
               ruby-shindo
Standards-Version: 3.9.7
Vcs-Git: https://anonscm.debian.org/git/pkg-ruby-extras/ruby-fog-voxel.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-ruby-extras/ruby-fog-voxel.git
Homepage: https://github.com/fog/fog-voxel
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all

Package: ruby-fog-voxel
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-fog-core,
         ruby-fog-xml,
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: ruby-fog (<< 1.25~)
Replaces: ruby-fog (<< 1.25~)
Description: module for the 'fog' gem to support Voxel
 This library can be used as a module for `fog` or as standalone provider
 to use the Voxel in applications. fog is the Ruby cloud services library,
 top to bottom:
 .
  *Collections provide a simplified interface, making clouds easier to work with
  and switch between.
 .
  *Requests allow power users to get the most out of the features of each
  individual cloud.
 .
  *Mocks make testing and integrating a breeze.
